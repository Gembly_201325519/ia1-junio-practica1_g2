%Primera Temporada Game of Thrones
%
:- discontiguous
        hombre/1,
        mujer/1,
	padres/3,
	papa/2,
	mama/2,
	padre/2,
	hermanos/2.

/*Starks*/
hombre(benjen).
hombre(eddard).
hombre(robb).
mujer(sansa).
mujer(arya).
hombre(brandon).
hombre(rickon).
hombre(jon_snow).

/*Tullys*/
mujer(catelyn).
mujer(lysa).

/*Lannisters*/
hombre(tywin).
hombre(jaime).
mujer(cersei).
hombre(tyrion).

/*Baratheon*/
hombre(robert).
hombre(stannis).
hombre(joffrey).
mujer(myrcella).
hombre(tommen).

/*Targaryens*/
mujer(daenerys).
hombre(viserys).
hombre(aemon).

/*Arryn*/
hombre(jon).
hombre(robin).

/*Mormont*/
hombre(jeor).
hombre(jorah).
mujer(lyanna).

/*Otros*/
hombre(khal_drogo).
dragon(viserion).
dragon(drogon).
dragon(rhaegal).
muertos(whitewalkers).
hombre(little_finger).
hombre(varys).
hombre(renly).

/*Casas*/
house(stark).
house(tully).
house(lannister).
house(baratheon).
house(targaryen).
house(arryn).
house(mormont).

apellido(benjen, stark).
apellido(eddard, stark).
apellido(robb, stark).
apellido(sansa, stark).
apellido(arya, stark).
apellido(brandon, stark).
apellido(rickon, stark).
apellido(jon_snow, stark).

apellido(catelyn, tully).

apellido(tywin, lannister).
apellido(jaime, lannister).
apellido(cersei, lannister).
apellido(tyrion, lannister).

apellido(robert, baratheon).
apellido(stannis, baratheon).
apellido(joffrey, baratheon).
apellido(myrcella, baratheon).
apellido(tommen, baratheon).

apellido(viserys, targaryen).
apellido(daenerys, targaryen).
apellido(aemon, targaryen).

apellido(jon, arryn).
apellido(robin, arryn).

/*Padres*/
padres(eddard, catelyn, robb).
padres(eddard, catelyn, sansa).
padres(eddard, catelyn, arya).
padres(eddard, catelyn, brandon).
padres(eddard, catelyn, rickon).
papa(eddard, jon_snow).
padres(robert, cersei, joffrey).
padres(robert, cersei, myrcella).
padres(robert, cersei, tommen).
papa(tywin, jaime).
papa(tywin, cersei).
papa(tywin, tyrion).
mama(daenerys, viserion).
mama(daenerys, drogon).
mama(daenerys, rhaegal).
padres(jon, lysa, robin).

papa(Padre,Hijo) :- padres(Padre,_,Hijo).
mama(Madre,Hijo) :- padres(_,Madre,Hijo).

nomama(Hijo):- papa(_,Hijo),not(mama(_,Hijo)).
nopapa(Hijo):- mama(_,Hijo),not(papa(_,Hijo)).

nightwatch(benjen).
nightwatch(jon_snow).
nightwatch(aemon).
nightwatch(jeor).

king(robert).
hand_of_the_king(jon, robert).
hand_of_the_king(eddard, robert).

direWolf(summer).
direWolf(ghost).
direWolf(shaggydog).
direWolf(nymeria).
direWolf(lady).
direWolf(gray_wind).

mascota(summer, brandon).
mascota(grey_wind, robb).
mascota(nymeria, arya).
mascota(lady, sansa).
mascota(ghost, jon_snow).
mascota(shaggydog, rickon).

aconsejan_a(little_finger).
aconsejan_a(varys).
aconsejan_a(renly).

aconsejan_a(Persona) :- hand_of_the_king(Persona, _).
padre(Persona,Hijo) :- papa(Persona,Hijo); mama(Persona,Hijo).

hermanos(Persona1, Persona2) :- ((padres(Papa, Mama, Persona1), padres(Papa, Mama, Persona2), Persona1 \= Persona2)).

